# Clone
- `git clone https://gitlab.com/AndrewBalakirev/tor-proxy-into-docker.git`

# How run?
- `cd tor-proxy-into-docker`

### Use docker compose
- `docker compose up -d`

### Use docker
- `docker build -t tor-proxy .`
- `docker run -p 9050:9050 --rm tor-proxy /usr/bin/tor -f /etc/tor/torrc`

# Usage
- set proxy `socks://127.0.0.1:9050` to your system/browser/program
- Have FUN!
