FROM ubuntu

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# Update Ubuntu Software repository
RUN apt update && \
    apt upgrade -y && \
    apt install -y tor

# Clean
RUN rm -rf /var/lib/apt/lists/* && apt clean

COPY ./torrc /etc/tor/torrc
